const $utils = require('./build.utils')

const argv = $utils.argv

if (argv.g6) {
    return module.exports = require('./build.g6')
}

if (!argv.release) {
    return module.exports = require('./docs')
} else {
    return module.exports = require('./release')
}
