const $utils = require('./build.utils')

module.exports = {
    publicPath: './',
    outputDir: 'docs',
    devServer: {
        port: '4488'
    },
    pages: {
        index: {
            entry: $utils.resolve('doc/main.ts'),
            template: 'public/index.html',
            filename: 'index.html',
            title: 'vue-flow-editor',
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    },
}