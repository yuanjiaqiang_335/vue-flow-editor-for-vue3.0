const $utils = require('./build.utils')

console.log('build umd')

module.exports = {
    publicPath: './',
    outputDir: 'docs/dist',
    configureWebpack: {
        entry: {
            'vue-flow-editor': $utils.resolve('src/index.ts')
        },
        output: {
            filename: `vue-flow-editor.js`,
            libraryTarget: 'umd',
            libraryExport: 'default',
            library: ['VueFlowEditor'],
            globalObject: 'this'
        },
    },
    css: {
        sourceMap: true,
        extract: {
            filename: `[name].css`
        }
    },
    chainWebpack: config => {
        config.optimization.delete('splitChunks')
        config.plugins.delete('copy')
        config.plugins.delete('preload')
        config.plugins.delete('prefetch')
        config.plugins.delete('html')
        config.plugins.delete('hmr')
        config.entryPoints.delete('app')
    }
}