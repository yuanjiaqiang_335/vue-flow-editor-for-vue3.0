import { createApp ,h } from 'vue';

import App from './App.vue'

import VueFlowEditor from '../src/index'
import 'element-plus/lib/theme-chalk/index.css'
import element from 'element-plus'

const Vue = createApp({
    render: ()=>h(App)
});

Vue.use(VueFlowEditor)
// @ts-ignore
//Vue.use(window.ELEMENT)
//Vue.config.productionTip = false
Vue.use(element)

Vue.mount("#app")