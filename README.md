# vue-flow-editor-vue3.0

## 简介

vue-flow-editor 是基于 Vue3.0 以及 G6@3.1.10 实现的流程编辑器。在线操作地址：

## 特点

- 兼容ie；
- 轻量级依赖
    - 依赖于 Vue3.0、g6、element-plus，但是打包的 vue-flow-editor 中是不包含这些依赖的，
    由开发者手动引入，方便管理依赖；
    - 对G6的连接线进行了简单封装，对节点无影响。开发者可以使用G6的默认节点以实现自定义的样式；
    - 操作丰富，请见操作手册；
- 界面简洁，左侧菜单栏以及顶部工具栏都有插槽供开发者自定义内容；
- 内置一个右侧弹出框，开发者可以通过插槽，以及手动打开这个弹出框以编辑节点内容；

## 安装

### npm安装

npm i 所有包我都配置好了，记住不要用cnpm i，不然有个包下不下来

装好执行 npm run start 就行了

### 静态引入

参考docs\demo.html，还是2.0的要用自己改一下，只是把非静态引入的改了

## vue-flow-editor 属性说明

|属性名称|类型|默认值|可选值|说明|
| --- | --- | --- | --- | --- |
| data | object | --- | --- | 渲染的数据，数据格式参考G6文档 |
| grid | boolean | true | --- | 是否开启网格 |
| miniMap | boolean | false | --- | 是否开启缩略图 |
| --- | --- | --- | --- | --- |
| disabledDragEdge | boolean | --- | --- | 是否禁用拖拽连线 |
| disabledUndo | boolean | --- | --- | 是否禁用撤销/重做 |
| --- | --- | --- | --- | --- |
| height| number,string | 100%| --- | 画布高度 |
| toolbarHeight | string,number | 50px | --- | 工具栏高度 |
| menuWidth | string,number | 250px | --- | 菜单栏宽度 |
| modelWidth | string,number | 500px | --- | 右侧弹框宽度 |
| --- | --- | --- | --- | --- |
| onRef | (editor:object null )=>void | --- | --- | 获取组件的引用时，传递的函数，参数为组件暴露的函数以及响应式变量 |
| toolbarButtonHandler | (object:[])=>object[] | --- | --- | 对工具栏按钮做格式化的函数 |
| loading | boolean | --- | --- | 开始编辑器的loading状态 |
| multipleSelect | boolean | true | --- | 编辑器是否可以多选 |
| --- | --- | --- | --- | --- |
| beforeDelete | (model:object)=>Promise<any> | --- | --- | 删除前校验 |
| afterDelete | (model:object)=>Promise<any> | --- | --- | 删除后动作 |
| beforeAdd | (model:object)=>Promise<any> | --- | --- | 添加前校验 |
| afterAdd | (model:object)=>Promise<any> | --- | --- | 添加后动作 |

## 通过 onRef 得到的对象

|属性名称|类型|说明|
| --- | --- | --- |
| editorState | object | 编辑器组件内部状态变量 |
| commander | object | 命令对象，画布中所有操作都是通过commander进行的，以便实现撤销以及重做 |
| openModel | function | 打开右侧弹框 |
| closeModel | function | 关闭右侧弹框 |
| updateModel | function | 更新model |
| openPreview | function | 打开预览对话框 |

## 操作手册

### 工具栏

从左至右依次为：
- 开启/关闭网格；
- 开启/关闭缩略图；
- 适应画布（画布缩放适应界面大小）；
- 实际尺寸（画布中元素大小为实际像素大小）；
- 撤销；
- 重做；
- 放大；
- 缩小；
- 删除（选中节点之后删除）；
- 预览（在弹框中以适应画布的模式展示画布内容）；

### 选择

- 单击 节点/边 可以选中，选中的时候样式会有变化；
- 摁住 shift单击 可以选中多个 节点/边；
- 摁住 shift可以拖拽生成一个选择框，框内的 节点/边 会被选中；
- 鼠标在画布内快捷键 CTRL+A 可以全选；
- 多选的时候拖拽节点，会拖拽所有的节点；

### 新建节点/边

- 从左侧选项菜单列表中拖拽菜单到画布可以添加节点；
- 悬浮在节点内，会显示用来连接线的指示节点，点击拖拽连接其他节点的指示节点可以新建连线（这个不是特别灵敏，拖拽指示节点的时候稍微慢一点）；
- 右侧选项菜单中有各种形状的节点供拖拽展示；

### 编辑

- 双击节点会弹出编辑器右侧的编辑框（这个动作是开发者自定义的，可以自定义弹框编辑节点内容）；
- 编辑器右侧的编辑框内的内容是自定义的，可以根据节点内容动态变化，显示不同的表单内容；比如实例中，【开始】以及【结束】节点就只能编辑节点名称；而【审批节点】可以从下拉选项中选择【节点任务内容】；

# 后续待完成的功能

- 节点增加宽度以及高度的功能；
- 节点复制；

# 其他

- vue-flow-editor是基于 Vue3.0 + typescript 实现的库
- 如遇到元素突然不能拖拽的话，先点击一下旁边画布，再重新点击元素就可拖拽了   