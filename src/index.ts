import {EditorComponent} from './editor'
import {externalComponents} from "@/components";
import VueFlowEditor from './editor/vue-flow-editor'
import VueFLowEditMenu from './editor/vue-flow-edit-menu.vue'
import VueFLowEditMenuGroup from './editor/vue-flow-edit-menu-group.vue'
import {formatPos} from "@/utils/utils";

export default {
    VueFlowEditor,
    VueFLowEditMenu,
    VueFLowEditMenuGroup,
    formatPos,
    install(Vue: any) {
        [...externalComponents, ...EditorComponent].forEach(Component => {
            Vue.component(Component.name, Component)
        })
    }
}