import {registerAnchor} from "@/shape/anchor";
import {registerEdge} from "@/shape/edge";
import {registerActivity} from "@/shape/activity";

export function registerShape(G6) {
    registerAnchor(G6)
    registerEdge(G6)
    registerActivity(G6)
}